import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'front';
  token = ''

  ngOnInit() {
    this.token = localStorage.getItem('token')
  }

  deleteToken() {
    localStorage.removeItem('token')
  }

}

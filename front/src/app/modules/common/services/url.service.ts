import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UrlService {

    private readonly baseUrl = 'http://127.0.0.1:8000/api';

    private readonly url = {
        athlete: `${this.baseUrl}/athlete`
    };

  getUrl(path: string) {
    return this.url[path];
  }

  constructor() { }
}

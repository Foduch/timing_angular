import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { MaterialModule } from '../material/material.module';
import { FormsModule } from '@angular/forms';

import { RaceRoutingModule } from './race-routing.module'
import { RaceEditComponent } from './components/race-edit/race-edit.component'
import { RaceListComponent } from './components/race-list/race-list.component'
import { RaceViewComponent } from './components/race-view/race-view.component'
import { RacersAddComponent } from './components/racers-add/racers-add.component';
import { GroupAddComponent } from './components/group-add-window/group-add-window.component';
import { RacerSelectGroupComponent } from './components/racer-select-group/racer-select-group.component';
import { AssignComponent } from './components/assign-dialog/assign-dialog.component';

@NgModule({
  declarations: [RaceEditComponent, RaceListComponent, RaceViewComponent, RacersAddComponent, GroupAddComponent,
    RacerSelectGroupComponent, AssignComponent],
  imports: [
    CommonModule,
    RaceRoutingModule,
    MaterialModule,
    FormsModule
  ],
  entryComponents: [RacersAddComponent, GroupAddComponent, RacerSelectGroupComponent, AssignComponent]
})
export class RaceModule { }

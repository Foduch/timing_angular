import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { AthleteService } from '../../../athlete/services/athlete.service'
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog'
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component'
import { RaceService } from '../../services/race.service';
import { RacerSelectGroupComponent } from '../racer-select-group/racer-select-group.component';
import { TouchSequence } from 'selenium-webdriver';



@Component({
  selector: 'app-racers-add',
  templateUrl: './racers-add.component.html',
  styleUrls: ['./racers-add.component.css']
})
export class RacersAddComponent implements OnInit {

  athletes
  participants
  filterData = {
    firstName: '',
    lastName: '',
    gender: '',
    minAge: 0,
    maxAge: 120
  }
  genders = [{value: 'm', viewValue: 'Мужской'}, {value: 'f', viewValue: 'Женский'}]

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private _athleteService: AthleteService,
    private _raceService: RaceService,
    private _snack: SnackbarComponent,
    private _router: Router,
    private _route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.participants = this.data.participants
    this._athleteService.getList().subscribe(
      (result) => {
        this.athletes = result
        this.isInRace()
      }
    )
  }

  add(athlete) {
    let data = this.data
    data.athlete = athlete
    const dialogRef = this.dialog.open(RacerSelectGroupComponent, {data: data, width: '500px'})
    dialogRef.afterClosed().subscribe (result => {
      this._raceService.getParticipants(this.data.race.guid).subscribe(
          (result) => {
            this.participants = result
            this.isInRace()
          }
        )
    })
  }

  isInRace() {
    for (let index = 0; index < this.athletes.length; index ++) {
      if (this.participants.find(p => p.athlete === this.athletes[index].guid)) {
        this.athletes[index].inRace = true
      }
      else {
        this.athletes[index].inRace = false
      }
    }
  }

  filter() {
    this._athleteService.filter(this.filterData).subscribe(
      result => {
        this.athletes = result
        this.isInRace()
      },
      error => this._snack.showMessage('Не получилось отфильтровать')
    )
  }

  resetFilter() {
    this.filterData = {
      firstName: '',
      lastName: '',
      gender: '',
      minAge: 0,
      maxAge: 120
    }
    this._athleteService.getList().subscribe(
      result => {
        this.athletes = result
        this.isInRace()
      },
      error => this._snack.showMessage('Не получилось очистить')
    )
  }

}
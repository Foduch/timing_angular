import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog'
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component'
import { RaceService } from '../../services/race.service'


@Component({
  selector: 'app-racer-select-group',
  templateUrl: './racer-select-group.component.html',
  styleUrls: ['./racer-select-group.component.css']
})
export class RacerSelectGroupComponent implements OnInit {

    group
    groups


  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private _raceService: RaceService,
    private _snack: SnackbarComponent,
    public dialog: MatDialogRef<RacerSelectGroupComponent>
  ) { }

  ngOnInit() {
      this.groups = this.data.groups.filter(item => item.gender == this.data.athlete.gender)
  }

  add() {
    let data = {
          athlete: this.data.athlete.guid,
          race: this.data.race.guid,
          group: this.group
        }
    this._raceService.addParticipant(this.data.race.guid, data).subscribe(
        (result) => {
            this._snack.showMessage('Участник добавлен')
            this.dialog.close()
        },
        (err) => {this._snack.showMessage('Ошибка')}
    )
      }
    // let data = {
    //   name: this.group.name,
    //   gender: this.group.gender,
    //   start_time: date,
    //   race: this.data.guid
    // }
    //   this._raceService.addGroup(this.data.guid, data).subscribe(
    //       (result) => {
    //         this._snack.showMessage('Категория добавлена')
    //         this.dialog.closeAll()
    //       },
    //       (err) => {this._snack.showMessage('Ошибка')}
    //   )
//   }

}
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RaceService } from '../../services/race.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment'

import { RaceModel } from '../../../../models/race.model'
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component'


@Component({
  selector: 'app-race-edit',
  templateUrl: './race-edit.component.html',
  styleUrls: ['./race-edit.component.css']
})
export class RaceEditComponent implements OnInit {

  @Input('raceId')
  raceId : string
  @Output('editMode')
  editMode = new EventEmitter<boolean>()

  
  startDate = moment().format('DD.MM.YYYY HH:mm')
  date = new Date()
  race: RaceModel = {name: '', race_type: '', start_date_time: this.date}
  genders = [{value: 'm', viewValue: 'Мужской'}, {value: 'f', viewValue: 'Женский'}]


  constructor(
    private _service: RaceService,
    private _snack: SnackbarComponent,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this._route.params.subscribe((params) => {
      if (params['guid']) {
        this._service.getOne(params['guid']).subscribe(
          (result) => {
            this.race = result
            this.startDate = moment(result.start_date_time).format('DD.MM.YYYY HH:mm')
            this.race.start_date_time = new Date(result.start_date_time)
          }
        )
      }
    })
  }

  save(formData) {
    let re = /\d+\.\d+\.\d+ \d+:\d+/
    if (!re.test(this.startDate)) {
      formData.form.controls['start_date_time'].setErrors({'format': true})
      return
    }
    let date = moment(this.startDate, 'DD.MM.YYYY HH:mm')
    let data = JSON.parse(JSON.stringify(this.race))
    data.start_date_time = date
    if (data.guid) {
      this._service.update(data).subscribe((result) => {
        this._snack.showMessage('Данные обновлены')
        this.editMode.emit(false)
        },
        (err) => {
          this._snack.showMessage(err.message)
        }
      )
    } else {
      this._service.create(data).subscribe((result) => {
        this._snack.showMessage('Гонка успешно создана')
        this._router.navigate(['/race'])
        },
        (err) => {
          this._snack.showMessage(err.message)
        }
      )
    }
  }

  cancelEdit() {
    this.editMode.emit(false)
  }
  
}

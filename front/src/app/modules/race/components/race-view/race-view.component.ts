import { Component, OnInit } from '@angular/core';
import { RaceService } from '../../services/race.service'
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog'
import { RaceModel } from '../../../../models/race.model'
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component'
import { RacersAddComponent } from '../racers-add/racers-add.component';
import { GroupAddComponent } from '../group-add-window/group-add-window.component';
import { AssignComponent } from '../assign-dialog/assign-dialog.component';


@Component({
  selector: 'app-race-view',
  templateUrl: './race-view.component.html',
  styleUrls: ['./race-view.component.css']
})
export class RaceViewComponent implements OnInit {

  editMode = false
  race: RaceModel
  groups
  participants
  results
  objectKeys = Object.keys


  constructor(
    private _service: RaceService,
    private _snack: SnackbarComponent,
    private _router: Router,
    private _route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this._route.params.subscribe((params) => {
      if (params['guid']) {
        this._service.getOne(params['guid']).subscribe(
          (result) => {
            this.race = result
          }
        )
        this._service.getGroups(params['guid']).subscribe(
          (result) => {
            this.groups = result
          }
        )
        this._service.getParticipants(params['guid']).subscribe(
          (result) => {
            this.participants = result
          }
        )
        this._service.getResults(params['guid']).subscribe(
          (result) => {
            this.results = result
          }
        )
      }
    })
  }

  enableEdit() {
    this.editMode = true
  }

  add() {
    let data = {race: this.race, groups: this.groups, participants: this.participants}
    const dialogRef = this.dialog.open(RacersAddComponent, {data: data, height: '500px'})
    dialogRef.afterClosed().subscribe (result => {
      this._service.getParticipants(this.race.guid).subscribe(
          (result) => {
            this.participants = result
          }
        )
    })
  }

  addGroup() {
    const dialogRef = this.dialog.open(GroupAddComponent, {data: this.race, width: '500px'})
    dialogRef.afterClosed().subscribe (result => {
      this._service.getGroups(this.race.guid).subscribe(
          (result) => {
            this.groups = result
          }
        )
    })
  }

  openAssign(participant) {
    let data = {race: this.race, participant}
    const dialogRef = this.dialog.open(AssignComponent, {data: data, width: '500px'})
    dialogRef.afterClosed().subscribe (result => {
      this._service.getGroups(this.race.guid).subscribe(
          (result) => {
            this._service.getParticipants(this.race['guid']).subscribe(
              (result) => {
                this.participants = result
              }
            )
          }
        )
    })
  }

  getFirstName(partGuid) {
    let athlete_data = this.participants.find(item => item.guid === partGuid).athlete_data
    return athlete_data.first_name
  }

  getLastName(partGuid) {
    let athlete_data = this.participants.find(item => item.guid === partGuid).athlete_data
    return athlete_data.last_name
  }

  disableEdit() {
    this.editMode = false
  }

}
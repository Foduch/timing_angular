import { Component, OnInit } from '@angular/core';

import { RaceService } from '../../services/race.service'
import { RaceModel } from 'src/app/models/race.model'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-race-list',
  templateUrl: './race-list.component.html',
  styleUrls: ['./race-list.component.css']
})
export class RaceListComponent implements OnInit {

  races$: Observable<RaceModel[]>

  constructor(private _service: RaceService) { }

  ngOnInit() {
    this.races$ = this._service.getList()
  }

}

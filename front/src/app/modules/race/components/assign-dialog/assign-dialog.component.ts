import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { MatDialogRef } from '@angular/material/dialog'
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component'
import { RaceService } from '../../services/race.service';


@Component({
  selector: 'app-assign',
  templateUrl: './assign-dialog.component.html',
  styleUrls: ['./assign-dialog.component.css']
})
export class AssignComponent implements OnInit {

  participant

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private _raceService: RaceService,
    private _snack: SnackbarComponent,
    public dialog: MatDialogRef<AssignComponent>
  ) { }

  ngOnInit() {
    this.participant = this.data.participant
  }

  assign(formData) {
    let isNegative = this.isNegative(formData, this.participant.number)
    let hasDigits = this.hasDigits(formData, this.participant.number)
    if (isNegative || hasDigits) return
    if (!this.participant.number) {
        this.participant.number = 0
    }
    if (!this.participant.tag) {
        this.participant.tag = ''
    }
    this._raceService.editParticipant(this.data.race.guid, this.participant).subscribe(
        (result) => {
            this._snack.showMessage('Данные обновлены')
            this.dialog.close()
        },
        (error) => {
            if (error.error.message === 'number already in use') formData.form.controls['number'].setErrors({'alreadyAssign': true})
            else {
              if (error.error.message === 'tag already in use') formData.form.controls['tag'].setErrors({'alreadyAssign': true})
              else this._snack.showMessage('Ошибка')
          }
        }
    )
  }

  hasDigits(formData, string: string) {
    let re = /\.|\,/
    if (re.test(string)) {
      formData.form.controls['number'].setErrors({'hasDigits': true})
      return true
    }
  }

  isNegative(formData, string) {
    let number = Number(string)
    if (number < 0) {
      formData.form.controls['number'].setErrors({'isNegative': true})
      return true
    }
  }

}
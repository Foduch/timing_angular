import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import { AthleteService } from '../../../athlete/services/athlete.service'
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog'
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component'
import { RaceService } from '../../services/race.service';
import * as moment from 'moment'


@Component({
  selector: 'group-add-window',
  templateUrl: './group-add-window.component.html',
  styleUrls: ['./group-add-window.component.css']
})
export class GroupAddComponent implements OnInit {

  startDate
  date = new Date()
  genders = [{value: 'm', viewValue: 'Мужской'}, {value: 'f', viewValue: 'Женский'}]

  group = {name: '', gender: '', start_time: ''}

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    private _raceService: RaceService,
    private _snack: SnackbarComponent,
    private _router: Router,
    private _route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.startDate = moment(this.data.start_date_time).format('DD.MM.YYYY HH:mm')
  }

  add(formData) {
    let re = /\d+\.\d+\.\d+ \d+:\d+/
    if (!re.test(this.startDate)) {
      formData.form.controls['start_date_time'].setErrors({'format': true})
      return
    }
    let date
    try {
      date = moment(this.startDate, 'DD.MM.YYYY HH:mm')      
    } catch (error) {
      formData.form.controls['start_date_time'].setErrors({'format': true})            
    }
    let data = {
      name: this.group.name,
      gender: this.group.gender,
      start_time: date,
      race: this.data.guid
    }
      this._raceService.addGroup(this.data.guid, data).subscribe(
          (result) => {
            this._snack.showMessage('Категория добавлена')
            this.dialog.closeAll()
          },
          (err) => {this._snack.showMessage('Ошибка')}
      )
  }

}
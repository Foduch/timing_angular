import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RaceEditComponent } from './components/race-edit/race-edit.component'
import { RaceListComponent } from './components/race-list/race-list.component'
import { RaceViewComponent } from './components/race-view/race-view.component'


const routes = [
  {path: 'race', component: RaceListComponent},
  {path: 'race/new', component: RaceEditComponent},
  {path: 'race/:guid', component: RaceViewComponent}
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RaceRoutingModule { }

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RaceModel } from 'src/app/models/race.model';

@Injectable({
  providedIn: 'root'
})
export class RaceService {

  _baseUri = 'http://192.168.39.105:8000/api/race/'

  constructor(private _http: HttpClient) { }

  getList() {
    return this._http.get<RaceModel[]>(this._baseUri)
  }

  getOne(id: string) {
    return this._http.get<RaceModel>(this._baseUri + id)
  }

  create(data) {
    return this._http.post(this._baseUri, data)
  }

  update(data) {
    return this._http.put(this._baseUri + data.guid, data)
  }

  addParticipant(guid, data) {
    return this._http.post(this._baseUri + guid + '/participant/', data)
  }

  getGroups(guid) {
    return this._http.get(this._baseUri + guid + '/group/')
  }

  addGroup(guid, data) {
    return this._http.post(this._baseUri + guid + '/group/', data)
  }

  getParticipants(guid) {
    return this._http.get(this._baseUri + guid + '/participant/')
  }

  editParticipant(guid, participant) {
    return this._http.put(this._baseUri + guid + '/participant/' + participant.guid, participant)
  }

  getResults(guid) {
    return this._http.get(this._baseUri + guid + '/result/')
  }

}

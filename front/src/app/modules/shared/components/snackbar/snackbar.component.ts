import { Injectable, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material'


@Injectable({
    providedIn: 'root',
  })
export class SnackbarComponent implements OnInit {

  constructor(private _snack: MatSnackBar) { }

  ngOnInit() {}

  showMessage(message) {
    this._snack.open(message, undefined, {duration: 4000})
  }

}

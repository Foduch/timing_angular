import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { TokenLoginComponent } from './token-login/token.login.component'

@NgModule({
  declarations: [TokenLoginComponent],
  imports: [
    CommonModule
  ]
})
export class AuthModule { }

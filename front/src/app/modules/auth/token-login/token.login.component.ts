import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router, Params } from '@angular/router'
import { AuthService } from '../auth.service'

@Component({
  selector: 'app-token-login',
  templateUrl: './token-login.component.html',
  styleUrls: ['./token-login.component.css'],
  providers: [AuthService]
})
export class TokenLoginComponent implements OnInit {

  _code: string
  error = false

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _service: AuthService
    ) { }

  ngOnInit() {
    this._route.queryParams.subscribe((params: Params) => {
      this._code = params['code']
      if (this._code) {
        this._service.doTokenAuth(this._code).subscribe(
          (response) => {
            localStorage.setItem('token', response['token'])
            this._router.navigateByUrl('/')
          },
          (error) => {
            this.error = true
          }
        )
      }
    })
  }

}

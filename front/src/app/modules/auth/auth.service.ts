import { Injectable } from '@angular/core'
import { HttpClient, HttpParams } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private _http: HttpClient) { }

  doTokenAuth(code: string) {
    const params = new HttpParams().set('code', code)
    return this._http.get('http://127.0.0.1:8000/vk-login', {params})
  }
}

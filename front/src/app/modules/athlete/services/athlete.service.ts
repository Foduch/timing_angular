import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AthleteModel } from 'src/app/models/athlete.model';

@Injectable({
  providedIn: 'root'
})
export class AthleteService {

  _baseUri = 'http://192.168.39.105:8000/api/athlete/'

  constructor(private _http: HttpClient) { }

  getList() {
    return this._http.get<AthleteModel[]>(this._baseUri)
  }

  getOne(id: string) {
    return this._http.get<AthleteModel>(this._baseUri + id)
  }

  create(data) {
    return this._http.post(this._baseUri, data)
  }

  update(data) {
    return this._http.put(this._baseUri + data.guid, data)
  }

  delete(id: string) {
    return this._http.delete(this._baseUri + id)
  }

  filter(filter) {
    return this._http.get(this._baseUri + `?first_name=${filter.firstName}&last_name=${filter.lastName}`
      +`${filter.gender ? `&gender=${filter.gender}` : ''}&age_min=${filter.minAge}&age_max=${filter.maxAge}`)
  }
}

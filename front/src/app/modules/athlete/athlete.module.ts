import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AthleteListComponent } from './components/athlete-list/athlete-list.component';
import { AthleteListItemComponent } from './components/athlete-list-item/athlete-list-item.component';
import { AthleteProfileComponent, AthleteDeleteDialog } from './components/athlete-profile/athlete-profile.component'
import { AthleteRoutingModule } from './athlete-routing.module';
import { AthleteEditComponent } from './components/athlete-edit/athlete-edit.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AthleteListComponent, AthleteListItemComponent, AthleteEditComponent,
    AthleteProfileComponent, AthleteDeleteDialog],
  imports: [
    CommonModule,
    AthleteRoutingModule,
    MaterialModule,
    FormsModule
  ],
  entryComponents: [AthleteDeleteDialog]
})
export class AthleteModule { }

export interface Athlete {
    first_name: string;
    last_name: string;
    gender: string;
    birthday: Date;
    country: string;
    region: string;
    city: string;
}

export interface AthleteWithId extends Athlete {
    guid: string;
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AthleteListComponent } from './components/athlete-list/athlete-list.component'
import { AthleteEditComponent } from './components/athlete-edit/athlete-edit.component'
import { AthleteProfileComponent } from './components/athlete-profile/athlete-profile.component'


const routes = [
  {path: 'athlete', component: AthleteListComponent},
  {path: 'athlete/new', component: AthleteEditComponent},
  {path: 'athlete/:guid', component: AthleteProfileComponent}
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AthleteRoutingModule { }

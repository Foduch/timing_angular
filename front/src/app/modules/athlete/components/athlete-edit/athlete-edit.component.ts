import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AthleteService } from '../../services/athlete.service';
import { MatSnackBar } from '@angular/material'
import { Router, ActivatedRoute } from '@angular/router';

import { AthleteModel } from '../../../../models/athlete.model'
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component'


@Component({
  selector: 'app-athlete-edit',
  templateUrl: './athlete-edit.component.html',
  styleUrls: ['./athlete-edit.component.css']
})
export class AthleteEditComponent implements OnInit {

  @Input('id')
  id : string
  @Output('editMode')
  editMode = new EventEmitter<boolean>()

  startDate: Date = new Date(1995, 0, 1);
  athlete: AthleteModel = {first_name: '', last_name: '', gender: '', birthday: this.startDate}
  genders = [{value: 'm', viewValue: 'Мужской'}, {value: 'f', viewValue: 'Женский'}]


  constructor(
    private _service: AthleteService,
    private _snack: SnackbarComponent,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this._route.params.subscribe((params) => {
      if (params['guid']) {
        this._service.getOne(params['guid']).subscribe(
          (result) => {
            this.athlete = result
            this.athlete.birthday = new Date(result.birthday)
          }
        )
      }
    })
  }

  save() {
    let date = `${this.athlete.birthday.getFullYear()}-${this.athlete.birthday.getMonth() + 1}-${this.athlete.birthday.getDate()}`
    let data = JSON.parse(JSON.stringify(this.athlete))
    data.birthday = date
    if (data.guid) {
      this._service.update(data).subscribe((result) => {
        this._snack.showMessage('Данные обновлены')
        this.editMode.emit(false)
        },
        (err) => {
          this._snack.showMessage(err)
        }
      )
    } else {
      this._service.create(data).subscribe((result) => {
        this._snack.showMessage('Профиль успешно создан')
        this._router.navigate(['/athlete'])
        },
        (err) => {
          this._snack.showMessage(err)
        }
      )
    }
  }

  cancelEdit() {
    this.editMode.emit(false)
  }
  
}

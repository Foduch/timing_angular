import { Component, OnInit } from '@angular/core';
import { AthleteService } from '../../services/athlete.service'
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog'
import * as moment from 'moment'

import { AthleteModel } from '../../../../models/athlete.model'
import { SnackbarComponent } from '../../../shared/components/snackbar/snackbar.component'
import { Observable } from 'rxjs';


@Component({
  selector: 'app-athlete-profile',
  templateUrl: './athlete-profile.component.html',
  styleUrls: ['./athlete-profile.component.css']
})
export class AthleteProfileComponent implements OnInit {

  editMode = false
  athlete: AthleteModel
  genders = {'m': 'Мужчина', 'f': 'Женщина'}
  dateString
  athlete$: Observable<AthleteModel>


  constructor(
    private _service: AthleteService,
    private _snack: SnackbarComponent,
    private _router: Router,
    private _route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this._route.params.subscribe((params) => {
      if (params['guid']) {
        this.athlete$ = this._service.getOne(params['guid'])
        this.athlete$.subscribe(result => this.athlete = result)
      }
    })
  }

  delete() {
    const dialogRef = this.dialog.open(AthleteDeleteDialog)
    dialogRef.afterClosed().subscribe (result => {
      if (result === true) this._service.delete(this.athlete.guid).subscribe(
        (response) => {
          this._snack.showMessage('Спортсмен удален')
          this._router.navigate(['/athlete'])
        },
        (err) => {
          this._snack.showMessage(err.statusText)
        }
      )
    })
  }

  enableEdit() {
    this.editMode = true
  }

  disableEit() {
    this.editMode = false
  }

}


@Component({
  selector: 'app-athlete-delete-dialog',
  templateUrl: 'athlete-delete.component.html',
})
export class AthleteDeleteDialog {}


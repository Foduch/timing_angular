import { Component, OnInit } from '@angular/core';

import { AthleteService } from '../../services/athlete.service'
import { AthleteModel } from 'src/app/models/athlete.model'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-athlete-list',
  templateUrl: './athlete-list.component.html',
  styleUrls: ['./athlete-list.component.css']
})
export class AthleteListComponent implements OnInit {

  athletes$: Observable<AthleteModel[]>

  constructor(private _service: AthleteService) { }

  ngOnInit() {
    this.athletes$ = this._service.getList()
  }

}

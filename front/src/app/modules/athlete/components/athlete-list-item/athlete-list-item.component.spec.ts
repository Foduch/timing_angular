import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AthleteListItemComponent } from './athlete-list-item.component';

describe('AthleteListItemComponent', () => {
  let component: AthleteListItemComponent;
  let fixture: ComponentFixture<AthleteListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AthleteListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AthleteListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment'

@Component({
  selector: 'app-athlete-list-item',
  templateUrl: './athlete-list-item.component.html',
  styleUrls: ['./athlete-list-item.component.css']
})
export class AthleteListItemComponent implements OnInit {
  dateString

  @Input() athlete

  constructor() { }

  ngOnInit() {
    this.dateString = moment(this.athlete.birthday).format('DD.MM.YYYY')
  }

}

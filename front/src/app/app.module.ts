import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { AthleteModule } from './modules/athlete/athlete.module'
import { MaterialModule } from './modules/material/material.module'
import { AppHttpInterceptor } from './app.interceptor'
import { SharedModule } from './modules/shared/shared.module'
import { RaceModule } from './modules/race/race.module'
import { AuthModule } from './modules/auth/auth.module';
import { MAT_DATE_LOCALE } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AthleteModule,
    AuthModule,
    MaterialModule,
    HttpClientModule,
    SharedModule,
    RaceModule
  ],
  providers: [ 
  {provide: MAT_DATE_LOCALE, useValue: 'ru-RU'},
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AppHttpInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { TokenLoginComponent } from './modules/auth/token-login/token.login.component'

const routes: Routes = [
  {path: 'login/vk/token', component: TokenLoginComponent}

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

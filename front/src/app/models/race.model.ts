export interface RaceModel {
    guid?: string
    name: string
    start_date_time: Date
    race_type: string
    description?: string
    region?: string
    city?: string
    status?: string
    is_deleted?: boolean
}
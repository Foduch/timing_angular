export interface Group {
    guid?: string
    race: string
    name: string
    gender: string
    age_min: number
    age_max: number
    laps_max: number
    start_time: Date
}
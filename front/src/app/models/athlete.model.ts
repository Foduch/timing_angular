export interface AthleteModel {
    guid?: string
    first_name: string
    last_name: string
    gender: string
    birthday: Date
    country?: string
    region?: string
    city?: string
}
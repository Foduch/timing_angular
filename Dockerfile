FROM node:11 AS build

WORKDIR /app

COPY front/angular.json .
COPY front/tsconfig.json .
COPY front/tslint.json .
COPY front/package.json .
COPY front/proxy.conf.json .

RUN npm install


COPY front/src/. ./src

RUN node ./node_modules/@angular/cli/bin/ng build front --configuration=production --build-optimizer=false

FROM nginx AS runtime

COPY --from=build /app/dist/front /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf